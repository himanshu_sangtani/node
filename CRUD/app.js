const express = require('express');
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
const cors=require('cors');
const app=express()
mongoose.connect('mongodb://localhost:27017/Blog', { useNewUrlParser: true, useUnifiedTopology:true }, (err) => {
    if (!err) { console.log('MongoDB Connection Succeeded.') }
    else { console.log('Error in DB connection : ' + err) }
});
const port=process.env.PORT||9000;
app.use(cors());
app.use(bodyParser.json());
app.get('/',(req,res)=>{
    res.send('<h1>HEY THERE!</h1>')
})

const postRoutes=require('./routes/api/post')
app.use('/api/posts',postRoutes)
app.listen(port,()=>{
    console.log("Started on "+port)
})