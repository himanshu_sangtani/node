const express=require('express')
const router=express.Router()
const Post=require('../../models/Post')
router.get('/',(req,res)=>{
    Post.find()
    .then((posts)=>{
        res.json(posts)
    })
    .catch(err=>console.log(err))
});

router.post('/add',(req,res)=>{
    const title=req.body.title;
    const body=req.body.body;
   newPost= new Post({
        title:title,
        body:body
    })
    newPost.save()
    .then(post=>{
    res.json(post)
    })
    .catch(err=>console.log(err))
})

router.put('/update/:id',(req,res)=>{
    let id=req.params.id;
    Post.find(id)
    // .update({set:{title:title,body:body}})
    .then(post=>{
        post.title=req.body.title;
        post.body=req.body.body;
        post.save()
        .then(post=>{
        res.send({message:'Post Updated',
          status:'success',
          post:post  
        })
        })
        .catch(err=>console.log(err))
    })
    .catch(err=>console.log(err))
})

router.delete('/:id',(res,req)=>{
    let id = req.params.id;
    Post.find(id)
    .then(post=>{
        post.destroy()
        .then(post=>{
        res.send({message:'Post deleted',
          status:'success',
          post:post  
        })
        })
        .catch(err=>console.log(err))
    })
    .catch(err=>console.log(err))
})

module.exports=router
